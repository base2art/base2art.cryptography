﻿namespace Base2art.Cryptography
{
    using System;
    using System.Security.Cryptography;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class CryptorFeature
    {
        
        [Test]
        public void ShouldDecrypt_NullSettings()
        {
            Action cryptor = () => new Cryptor(null);
            cryptor.ShouldThrow<ArgumentNullException>();
        }
        
        [Test]
        public void ShouldDecrypt()
        {
            Mock<ICryptorSettings> settings = new Mock<ICryptorSettings>();
            settings.Setup(x => x.Secret).Returns("This is the secret");
            
            var cryptor = new Cryptor(settings.Object);
            
            var baseValue = "ValueToEncrypt";
            var encryptedValue = cryptor.Encrypt<DESCryptoServiceProvider>(baseValue, "Salt Salt");
            
            var decryptedValue = cryptor.Decrypt<DESCryptoServiceProvider>(encryptedValue, "Salt Salt");
            
            decryptedValue.Should().Be(baseValue);
        }
        
        [Test]
        public void ShouldHash()
        {
            //ValueToEncrypt
            Mock<ICryptorSettings> settings = new Mock<ICryptorSettings>();
            settings.Setup(x => x.Secret).Returns("This is the secret");
            
            var cryptor = new Cryptor(settings.Object);
            
            var baseValue = "ValueToEncrypt";
            var encryptedValue = cryptor.Hash<MD5CryptoServiceProvider>(baseValue, "Salt Salt");
//            encryptedValue.ToLowerInvariant().Should().Be("a47f995d3c2ee7d259333dde8aac2fbd");
            encryptedValue.ToLowerInvariant().Should().Be("78b4248e58f5834331235c7bd78413d2");
            
        }
    }
}
