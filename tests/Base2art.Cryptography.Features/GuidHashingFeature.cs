﻿namespace Base2art.Cryptography
{
	using System;
	using System.Collections.Generic;
	using System.Linq.Expressions;
	using System.Security.Cryptography;
	using FluentAssertions;
	using NUnit.Framework;

	[TestFixture]
	public class GuidHashingFeature
	{
		[Test]
		public void ShouldHashCorrecty()
		{
			"base2art".HashAsMD5().AsString().Should().Be("47AEDA1582AE5E4838A1452F290A7F27");
			HashSet<string> set = new HashSet<string>();
			this.VerifyHash("base2art", set);
			this.VerifyHash("base2Art", set);
			for (int i = 'A'; i <= 'Z'; i++)
			{
				this.VerifyHash(new string((char)i, 1), set);
				this.VerifyHash(new string((char)i, 2), set);
				this.VerifyHash(new string((char)i, 3), set);
				this.VerifyHash(new string((char)i, 4), set);
			}
			for (int i = 'a'; i <= 'z'; i++)
			{
				this.VerifyHash(new string((char)i, 1), set);
				this.VerifyHash(new string((char)i, 2), set);
				this.VerifyHash(new string((char)i, 3), set);
				this.VerifyHash(new string((char)i, 4), set);
			}
		}

		private void VerifyHash(string value, HashSet<string> theSet)
		{
			var hashResult = value.HashAsMD5();
			var hashedValue = hashResult.AsString();
			if (theSet.Contains(hashedValue))
			{
				throw new Exception();
			}
			theSet.Add(hashedValue);
			hashResult.AsGuid().Should().Be(new Guid(hashedValue));
		}
	}
}


