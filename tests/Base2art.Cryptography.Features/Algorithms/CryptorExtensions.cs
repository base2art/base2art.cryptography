﻿namespace Base2art.Cryptography.Algorithms
{
    using System;
    using System.Collections.Generic;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class CryptorExtensions 
    {
        protected Mock<ICryptorSettings> cryptorSettings;

        protected Cryptor cryptor;
        
		[SetUp]
		public void BeforeEach()
		{
			this.cryptorSettings = new Mock<ICryptorSettings>(MockBehavior.Strict);
			this.cryptorSettings.Setup(x => x.Secret).Returns("My Secret");
			this.cryptor = new Cryptor(this.cryptorSettings.Object);
		}
		
        [Test, TestCaseSource("PasswordMatchSchemas")]
        public void ShouldProcess_WithUser_NullUri_UserFoundByName(
            string input, CryptoAlgorithmSchema userPasswordSchema, bool canDecrypt, bool match)
        {
            var salt = "SaltSALT";
            var algol = userPasswordSchema.GetAlgorithm();
            
            userPasswordSchema.IsHash().Should().Be(!canDecrypt);
            userPasswordSchema.IsEncryptor().Should().Be(canDecrypt);
            
            var encoded = algol.Encode(this.cryptor, input, salt);
            if (match)
            {
                encoded.Should().Be(input ?? string.Empty);
            }
            else
            {
                encoded.Should().NotBe(input);
            }
            
            algol.CanDecode.Should().Be(canDecrypt);
            
            
            if (algol.CanDecode)
            {
                var decoded = algol.Decode(this.cryptor, encoded, salt);
                decoded.Should().Be(input ?? string.Empty);
            }
            else
            {
                Action mut = () => algol.Decode(this.cryptor, encoded, salt);
                mut.ShouldThrow<InvalidOperationException>();
            }
        }
        
//        [Test, TestCaseSource("PasswordMatchSchemas")]
//        public void ShouldProcess_WithUser_NullUri_UserFoundByEmail(
//            string inputPassword, string userPassword, CryptoAlgorithmSchema userPasswordSchema, bool match)
//        {
////            this.cryptor.Setup(x => x.Hash<MD5CryptoServiceProvider>(It.IsAny<string>(), It.IsAny<string>()))
////                .Returns("hashedValue");
////            
////            this.authentication.Setup(x => x.HasUsers).Returns(true);
////            this.authentication.Setup(x => x.FindUserByEmail(It.IsAny<MailAddress>()))
////                .Returns(() => this.User(userPasswordSchema, userPassword));
////            
////            if (status != SignInStatus.Success)
////            {
////                this.authentication.Setup(x => x.IncreaseInvalidAttemptCount(It.IsAny<string>()));
////            }
////            
////            var result = this.processor.SignIn(this.UserData(inputPassword, null));
////            result.Should().Be(status);
//        }
        
        
        private IEnumerable<TestCaseData> PasswordMatchSchemas
        {
            get
            {
                var schemas = Enum.GetValues(typeof(CryptoAlgorithmSchema));
                
                yield return new TestCaseData("UserPassword", CryptoAlgorithmSchema.Encrypted_AES, true, false);
//                yield return new TestCaseData("UserPassword", (CryptoAlgorithmSchema)(-1), false, false);
                
                foreach (CryptoAlgorithmSchema userSchema in schemas)
                {
                    yield return new TestCaseData(
                        "InputPassword", 
                        userSchema, 
                        userSchema.ToString("G").StartsWith("Encrypted"), 
                        userSchema == CryptoAlgorithmSchema.PlainText);
                }
                
                foreach (CryptoAlgorithmSchema userSchema in schemas)
                {
                    yield return new TestCaseData(
                        "", 
                        userSchema, 
                        userSchema.ToString("G").StartsWith("Encrypted"), 
                        userSchema == CryptoAlgorithmSchema.PlainText);
                }
                
                foreach (CryptoAlgorithmSchema userSchema in schemas)
                {
                    yield return new TestCaseData(
                        null, 
                        userSchema, 
                        userSchema.ToString("G").StartsWith("Encrypted"), 
                        userSchema == CryptoAlgorithmSchema.PlainText);
                }
            }
        }
    }
}
