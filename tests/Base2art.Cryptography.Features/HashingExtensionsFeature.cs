﻿namespace Base2art.Cryptography
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Security.Cryptography;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class HashingExtensionsFeature
    {
        [TestCase("value", "secret", null, true)]
        [TestCase("value", "secret", "", true)]
        [TestCase("value", "secret", "salt", true)]
        
        
        [TestCase("value", "", "saltsalt", true)]
        [TestCase("value", null, "saltsalt", true)]
        
        [TestCase(null, "secret", "saltsalt", false)]
        [TestCase("", "secret", "saltsalt", false)]
        
        [TestCase("value", "secret", "saltsalt", false)]
        
        public void ShouldTestNull_Encrypt(string value, string secret, string salt, bool exception)
        {
            if (exception)
            {
                this.ExpectArgError(() => value.Encrypt<DESCryptoServiceProvider>(secret, salt));
            }
            else
            {
                this.ExpectNoError(()  => value.Encrypt<DESCryptoServiceProvider>(secret, salt));
            }
        }
        
        
        [TestCase("value", "secret", null, true)]
        [TestCase("value", "secret", "", true)]
        [TestCase("value", "secret", "salt", true)]
        
        
        [TestCase("value", "", "saltsalt", true)]
        [TestCase("value", null, "saltsalt", true)]
        
        [TestCase(null, "secret", "saltsalt", true)]
        [TestCase("", "secret", "saltsalt", true)]
        
        [TestCase("value", "secret", "saltsalt", false)]
        
        public void ShouldTestNull_Decrypt(string value, string secret, string salt, bool exception)
        {
            var encrypted = value.Encrypt<DESCryptoServiceProvider>("This is a test Secret", "SaltSalt")
                .AsString();
            
            if (string.IsNullOrWhiteSpace(value))
            {
                encrypted = value;
            }
            
            if (exception)
            {
                this.ExpectArgError(() => encrypted.Decrypt<DESCryptoServiceProvider>(secret, salt));
            }
            else
            {
                this.ExpectNoError(()  => encrypted.Decrypt<DESCryptoServiceProvider>(secret, salt));
            }
        }
        
        [Test]
        public void ShouldTestNull_Decrypt_Bytes()
        {
            byte[] item = null;
            this.ExpectArgError(() => item.Decrypt<DESCryptoServiceProvider>("My Secret", "Salt Salt"));
        }
        
        [TestCase("Base2art", "MY PERSONAL SECRET", "seaSalt!", "MY PERSONAL SECRET", "seaSalt!", true)]
        [TestCase("Base2art", "MY PERSONAL SECRET", "seaSalt!", "MY PERSONAL SECREt", "seaSalt!", false)]
        [TestCase("Base2art", "MY PERSONAL SECRET", "seaSalt!", "MY PERSONAL SECRET", "seaSalt*", false)]
        [TestCase("Base2art", "seaSalt!", "MY PERSONAL SECRET", "MY PERSONAL SECRET", "seaSalt!", false)]
        public void ShouldEncryptCorrecty(
            string input,
            string encryptSecret, string encryptSalt,
            string decryptSecret, string decryptSalt,
            bool shouldWork)
        {
            var encryptionResult = input.Encrypt<AesManaged>(encryptSecret, encryptSalt);
            var encryptedBytes = encryptionResult.AsByteArray();
            
            var output = encryptedBytes.Decrypt<AesManaged>(decryptSecret, decryptSalt);
            output.HasValue.Should().Be(shouldWork);
            
            if (shouldWork)
            {
                output.Value.AsString().Should().Be(input);
            }
            
            var encryptedString = encryptionResult.AsString();
            var outputFromString = encryptedString.Decrypt<AesManaged>(decryptSecret, decryptSalt);
            outputFromString.HasValue.Should().Be(shouldWork);
            
            if (shouldWork)
            {
                outputFromString.Value.AsString().Should().Be(input);
            }
        }
        
        [Test]
        public void ShouldHashCorrectly_AsByteArray()
        {
            Convert.ToBase64String("base2art".Hash<MD5Cng>().AsByteArray()).Should().Be("R67aFYKuXkg4oUUvKQp/Jw==");
        }
        
        [Test]
        public void ShouldHashCorrecty()
        {
            "base2art".Hash<MD5Cng>().AsString().Should().Be("47AEDA1582AE5E4838A1452F290A7F27");
            HashSet<string> set = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);
            this.VerifyHash("base2art", set);
            this.VerifyHash("base2Art", set);

            for (int i = 'A'; i <= 'Z'; i++)
            {
                this.VerifyHash(new string((char)i, 1), set);
                this.VerifyHash(new string((char)i, 2), set);
                this.VerifyHash(new string((char)i, 3), set);
                this.VerifyHash(new string((char)i, 4), set);
            }

            for (int i = 'a'; i <= 'z'; i++)
            {
                this.VerifyHash(new string((char)i, 1), set);
                this.VerifyHash(new string((char)i, 2), set);
                this.VerifyHash(new string((char)i, 3), set);
                this.VerifyHash(new string((char)i, 4), set);
            }
        }

        private void VerifyHash(string value, HashSet<string> theSet)
        {
            var hashResult = value.Hash<MD5Cng>();
            var hashedValue = hashResult.AsString();
            
            if (theSet.Contains(hashedValue))
            {
                throw new Exception();
            }

            theSet.Add(hashedValue);

            hashResult.AsString().Should().Be(hashedValue);
        }
        
        private void ExpectArgError(Action action)
        {
            action.ShouldThrow<ArgumentNullException>();
        }
        
        private void ExpectNoError(Action action)
        {
            action.ShouldNotThrow();
        }
    }
    
    
    
}
