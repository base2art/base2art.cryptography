﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Collections.ObjectModel;
    
    public class CryptoAlgorithmCollection : KeyedCollection<CryptoAlgorithmSchema, ICryptoAlgorithm>
    {
        protected override CryptoAlgorithmSchema GetKeyForItem(ICryptoAlgorithm item)
        {
            return item.CryptoAlgorithmSchema;
        }
    }
}
