﻿namespace Base2art.Cryptography.Algorithms
{
    public interface ICryptoAlgorithm
    {
        CryptoAlgorithmSchema CryptoAlgorithmSchema { get; }
        
        bool CanEncode { get; }
        
        bool CanDecode { get; }
        
        string Encode(ICryptor cryptographer, string value, string salt);
        
        string Decode(ICryptor cryptographer, string value, string salt);
    }
}
