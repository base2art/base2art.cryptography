﻿namespace Base2art.Cryptography.Algorithms
{
    public class PlainTextCryptoAlgorithm : HashCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.PlainText; }
        }
        
        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return value ?? string.Empty;
        }
    }
}
