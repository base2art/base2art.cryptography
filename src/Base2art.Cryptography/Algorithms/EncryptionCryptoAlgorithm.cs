﻿namespace Base2art.Cryptography.Algorithms
{
    public abstract class EncryptionCryptoAlgorithm : CryptoAlgorithm
    {
        protected sealed override bool CanEncode
        {
            get { return true; }
        }

        protected sealed override bool CanDecode
        {
            get { return true; }
        }
    }
}
