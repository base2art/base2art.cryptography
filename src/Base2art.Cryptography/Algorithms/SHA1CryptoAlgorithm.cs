﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class SHA1CryptoAlgorithm : HashCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Hashed_Sha1; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Hash<SHA1Managed>(value, salt);
        }
    }
}
