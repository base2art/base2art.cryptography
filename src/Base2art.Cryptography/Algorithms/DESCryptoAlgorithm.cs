﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class DESCryptoAlgorithm : EncryptionCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Encrypted_DES; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Encrypt<DESCryptoServiceProvider>(value, salt);
        }

        protected override string Decode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Decrypt<DESCryptoServiceProvider>(value, salt);
        }
    }
}
