﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class AESCryptoAlgorithm : EncryptionCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Encrypted_AES; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Encrypt<AesManaged>(value, salt);
        }
        
        protected override string Decode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Decrypt<AesManaged>(value, salt);
        }
    }
}
