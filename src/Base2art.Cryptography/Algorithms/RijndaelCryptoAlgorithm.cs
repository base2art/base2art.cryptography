﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class RijndaelCryptoAlgorithm : EncryptionCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Encrypted_Rijndael; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Encrypt<RijndaelManaged>(value, salt);
        }

        protected override string Decode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Decrypt<RijndaelManaged>(value, salt);
        }
    }
}
