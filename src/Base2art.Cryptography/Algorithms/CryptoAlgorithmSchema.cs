﻿namespace Base2art.Cryptography.Algorithms
{
    public enum CryptoAlgorithmSchema
    {
        PlainText,
        Hashed_MD5,
        Hashed_Sha1,
        Hashed_Sha256,
        Hashed_Sha512,
        Encrypted_AES,
        Encrypted_DES,
        Encrypted_RC2,
        Encrypted_Rijndael,
        Encrypted_TripleDES
    }
}
