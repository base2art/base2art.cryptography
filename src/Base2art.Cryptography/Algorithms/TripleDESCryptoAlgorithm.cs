﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class TripleDESCryptoAlgorithm : EncryptionCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Encrypted_TripleDES; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Encrypt<TripleDESCryptoServiceProvider>(value, salt);
        }

        protected override string Decode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Decrypt<TripleDESCryptoServiceProvider>(value, salt);
        }
    }
}
