﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Collections.ObjectModel;

    public static class CryptoAlgorithms
    {
        private static readonly KeyedCollection<CryptoAlgorithmSchema, ICryptoAlgorithm> BackingCryptoAlgorithms = new CryptoAlgorithmCollection()
        {
            new AESCryptoAlgorithm(),
            new DESCryptoAlgorithm(),
            new MD5CryptoAlgorithm(),
            new PlainTextCryptoAlgorithm(),
            new RC2CryptoAlgorithm(),
            new RijndaelCryptoAlgorithm(),
            new SHA1CryptoAlgorithm(),
            new SHA256CryptoAlgorithm(),
            new SHA512CryptoAlgorithm(),
            new TripleDESCryptoAlgorithm(),
        };

        public static ICryptoAlgorithm GetAlgorithm(this CryptoAlgorithmSchema value)
        {
            if (BackingCryptoAlgorithms.Contains(value))
            {
                return BackingCryptoAlgorithms[value];
            }
            
            return null;
        }
        
        public static bool IsHash(this CryptoAlgorithmSchema schema)
        {
            var algorithm = schema.GetAlgorithm();
            if (algorithm != null)
            {
                var algo = BackingCryptoAlgorithms[schema];
                return algo.CanEncode && !algo.CanDecode;
            }
            
            return false;
        }

        public static bool IsEncryptor(this CryptoAlgorithmSchema schema)
        {
            var algorithm = schema.GetAlgorithm();
            if (algorithm != null)
            {
                var algo = BackingCryptoAlgorithms[schema];
                return algo.CanEncode && algo.CanDecode;
            }
            
            return false;
        }
    }
}
