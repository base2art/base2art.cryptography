﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class RC2CryptoAlgorithm : EncryptionCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Encrypted_RC2; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Encrypt<RC2CryptoServiceProvider>(value, salt);
        }

        protected override string Decode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Decrypt<RC2CryptoServiceProvider>(value, salt);
        }
    }
}
