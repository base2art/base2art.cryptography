﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;
    
    public class MD5CryptoAlgorithm : HashCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Hashed_MD5; }
        }
        
        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Hash<MD5CryptoServiceProvider>(value, salt);
        }
    }
}
