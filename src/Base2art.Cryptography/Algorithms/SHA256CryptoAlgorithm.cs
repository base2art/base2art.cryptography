﻿namespace Base2art.Cryptography.Algorithms
{
    using System.Security.Cryptography;

    public class SHA256CryptoAlgorithm : HashCryptoAlgorithm
    {
        protected override CryptoAlgorithmSchema CryptoAlgorithmSchema
        {
            get { return CryptoAlgorithmSchema.Hashed_Sha256; }
        }

        protected override string Encode(ICryptor cryptographer, string value, string salt)
        {
            return cryptographer.Hash<SHA256Managed>(value, salt);
        }
    }
}
