﻿namespace Base2art.Cryptography.Algorithms
{
    using System;
    
    public abstract class HashCryptoAlgorithm : CryptoAlgorithm
    {
        protected sealed override bool CanEncode
        {
            get { return true; }
        }
        
        protected sealed override bool CanDecode
        {
            get { return false; }
        }
        
        protected sealed override string Decode(ICryptor cryptographer, string value, string salt)
        {
            throw new NotImplementedException();
        }
    }
}
