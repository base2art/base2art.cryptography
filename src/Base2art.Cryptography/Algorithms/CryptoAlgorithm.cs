﻿namespace Base2art.Cryptography.Algorithms
{
    using System;

    public abstract class CryptoAlgorithm : ICryptoAlgorithm
    {
        CryptoAlgorithmSchema ICryptoAlgorithm.CryptoAlgorithmSchema
        {
            get { return this.CryptoAlgorithmSchema; }
        }

        bool ICryptoAlgorithm.CanEncode
        {
            get { return this.CanEncode; }
        }

        bool ICryptoAlgorithm.CanDecode
        {
            get { return this.CanDecode; }
        }

        protected abstract CryptoAlgorithmSchema CryptoAlgorithmSchema { get; }

        protected abstract bool CanEncode { get; }

        protected abstract bool CanDecode { get; }

        string ICryptoAlgorithm.Encode(ICryptor cryptographer, string value, string salt)
        {
            if (cryptographer == null)
            {
                throw new ArgumentNullException("cryptographer");
            }
            
            if (!this.CanEncode)
            {
                throw new InvalidOperationException("Encoding is not allowed");
            }
            
            return this.Encode(cryptographer, value, salt);
        }

        string ICryptoAlgorithm.Decode(ICryptor cryptographer, string value, string salt)
        {
            if (cryptographer == null)
            {
                throw new ArgumentNullException("cryptographer");
            }
            
            if (!this.CanDecode)
            {
                throw new InvalidOperationException("Encoding is not allowed");
            }
            
            return this.Decode(cryptographer, value, salt);
        }

        protected abstract string Encode(ICryptor cryptographer, string value, string salt);

        protected abstract string Decode(ICryptor cryptographer, string value, string salt);
    }
}
