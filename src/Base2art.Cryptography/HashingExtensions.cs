﻿namespace Base2art.Cryptography
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    
    public static class HashingExtensions
    {
        public static IEncryptionResult Encrypt<T>(this string value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            return new EncryptionResult(EncryptStringAES<T>(value ?? string.Empty, secret, salt));
        }
        
        public static IDecryptionResult Decrypt<T>(this byte[] value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            try
            {
                var item = DecryptStringAES<T>(value, secret, salt);
                return new DecryptionResult(item);
            }
            catch (CryptographicException)
            {
                return new DecryptionResult(null);
            }
        }
        
        public static IDecryptionResult Decrypt<T>(this string value, string secret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException("value");
            }
            
            return Convert.FromBase64String(value).Decrypt<T>(secret, salt);
        }
        
        public static IHashResult Hash<T>(this string value)
            where T : HashAlgorithm, new()
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = new T())
            {
                return new HashResult(hashAlgorithm.ComputeHash(inputBytes));
            }
        }
        
        public static I16ByteHashResult HashAsMD5(this string value)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = new MD5CryptoServiceProvider())
            {
                return new Md5HashResult(hashAlgorithm.ComputeHash(inputBytes));
            }
        }
        
        /// <summary>
        /// Encrypt the given string using AES.  The string can be decrypted using
        /// DecryptStringAES().  The sharedSecret parameters must match.
        /// </summary>
        /// <param name="plainText">The text to encrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for encryption.</param>
        /// <param name = "salt">the items salt</param>
        private static byte[] EncryptStringAES<T>(string plainText, string sharedSecret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException("sharedSecret");
            }
            
            if (string.IsNullOrWhiteSpace(salt) || salt.Length < 8)
            {
                throw new ArgumentNullException("salt");
            }
            
            byte[] saltBytes = Encoding.ASCII.GetBytes(salt);
            
            using (T aesAlg = new T())
            {
                try
                {
                    using (var key = new Rfc2898DeriveBytes(sharedSecret, saltBytes))
                    {
                        aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    }
                    
                    using (ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV))
                    {
                        using (var msEncrypt = new MemoryStream())
                        {
                            msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                            msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                            {
                                using (var swEncrypt = new StreamWriter(csEncrypt))
                                {
                                    swEncrypt.Write(plainText);
                                }
                            }
                            
                            return msEncrypt.ToArray();
                        }
                    }
                }
                finally
                {
                    // Clear the RijndaelManaged object.
                    if (aesAlg != null)
                    {
                        aesAlg.Clear();
                    }
                }
            }
        }

        /// <summary>
        /// Decrypt the given string.  Assumes the string was encrypted using
        /// EncryptStringAES(), using an identical sharedSecret.
        /// </summary>
        /// <param name="cipherText">The text to decrypt.</param>
        /// <param name="sharedSecret">A password used to generate a key for decryption.</param>
        /// <param name = "salt">The salt</param>
        private static byte[] DecryptStringAES<T>(byte[] cipherText, string sharedSecret, string salt)
            where T : SymmetricAlgorithm, new()
        {
            if (cipherText == null)
            {
                throw new ArgumentNullException("cipherText");
            }
            
            if (string.IsNullOrEmpty(sharedSecret))
            {
                throw new ArgumentNullException("sharedSecret");
            }
            
            if (string.IsNullOrWhiteSpace(salt) || salt.Length < 8)
            {
                throw new ArgumentNullException("salt");
            }
            
            byte[] saltBytes = Encoding.ASCII.GetBytes(salt);
            
            // Declare the RijndaelManaged object
            // used to decrypt the data.
            using (T aesAlg = new T())
            {
                try
                {
                    // generate the key from the shared secret and the salt
                    using (var key = new Rfc2898DeriveBytes(sharedSecret, saltBytes))
                    {
                        using (var msDecrypt = new MemoryStream(cipherText))
                        {
                            msDecrypt.Seek(0, SeekOrigin.Begin);
                            
                            // Create a RijndaelManaged object
                            // with the specified key and IV.
                            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                            
                            // Get the initialization vector from the encrypted stream
                            var buf = new byte[4];
                            msDecrypt.Read(buf, 0, sizeof(int));
                            var ivLength = BitConverter.ToInt32(buf, 0);
                            
                            buf = new byte[ivLength];
                            
                            msDecrypt.Read(buf, 0, ivLength);
                            
                            aesAlg.IV = buf;
                            
                            // Create a decrytor to perform the stream transform.
                            using (var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV))
                            {
                                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                                {
                                    return csDecrypt.ReadFully();
                                }
                            }
                        }
                    }
                } 
                finally
                {
                    // Clear the RijndaelManaged object.
                    if (aesAlg != null)
                    {
                        aesAlg.Clear();
                    }
                }
            }
        }
        
        private static byte[] ReadFully(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                
                return ms.ToArray();
            }
        }
        
        private class EncryptionResult : IEncryptionResult
        {
            private readonly byte[] bytes;

            public EncryptionResult(byte[] bytes)
            {
                this.bytes = bytes;
            }
            
            public string AsString()
            {
                return Convert.ToBase64String(this.bytes);
            }

            public byte[] AsByteArray()
            {
                return this.bytes;
            }
        }
        
        private class DecryptionResult : IDecryptionResult
        {
            private readonly byte[] bytes;

            public DecryptionResult(byte[] bytes)
            {
                this.bytes = bytes;
            }
            
            public bool HasValue
            {
                get { return this.bytes != null; }
            }
            
            public IHashResult Value
            {
                get
                {
                    if (!this.HasValue)
                    {
                        throw new InvalidOperationException("No Value for bytes");
                    }
                    
                    return new StringResultValue(this.bytes);
                }
            }
            
            private class StringResultValue : IHashResult
            {
                private readonly Lazy<string> value;
                
                private byte[] bytes;

                public StringResultValue(byte[] bytes)
                {
                    this.bytes = bytes;
                    this.value = new Lazy<string>(this.Create);
                }

                public string AsString()
                {
                    return this.value.Value;
                }
                
                public byte[] AsByteArray()
                {
                    return this.bytes;
                }
                
                private string Create()
                {
                    using (var ms = new MemoryStream(this.bytes))
                    {
                        ms.Seek(0L, SeekOrigin.Begin);
                        
                        using (var sr = new StreamReader(ms))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }
            }
        }

        private class HashResult : IHashResult
        {
            private readonly byte[] hashedBytes;

            public HashResult(byte[] hashedBytes)
            {
                this.hashedBytes = hashedBytes;
            }

            protected byte[] HashedBytes
            {
                get { return this.hashedBytes; }
            }
            
            public byte[] AsByteArray()
            {
                return this.hashedBytes;
            }
            
            public string AsString()
            {
                var hash = this.hashedBytes;
                
                // step 2, convert byte array to hex string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2", CultureInfo.InvariantCulture));
                }
                
                return sb.ToString();
            }
        }
        
        private class Md5HashResult : HashResult, I16ByteHashResult
        {
            public Md5HashResult(byte[] hashedBytes)
                : base(hashedBytes)
            {
            }

            public Guid AsGuid()
            {
                var hashedBytes = this.HashedBytes;
                var bytes = new byte[16];
                bytes[0] = hashedBytes[3];
                bytes[1] = hashedBytes[2];
                bytes[2] = hashedBytes[1];
                bytes[3] = hashedBytes[0];
                bytes[4] = hashedBytes[5];
                bytes[5] = hashedBytes[4];
                bytes[6] = hashedBytes[7];
                bytes[7] = hashedBytes[6];
                bytes[8] = hashedBytes[8];
                bytes[9] = hashedBytes[9];
                bytes[10] = hashedBytes[10];
                bytes[11] = hashedBytes[11];
                bytes[12] = hashedBytes[12];
                bytes[13] = hashedBytes[13];
                bytes[14] = hashedBytes[14];
                bytes[15] = hashedBytes[15];
                return new Guid(bytes);
            }
        }
    }
}
