﻿namespace Base2art.Cryptography
{
    public interface IDecryptionResult
    {
        bool HasValue { get; }
        
        IHashResult Value { get; }
    }
}
