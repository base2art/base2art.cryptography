﻿namespace Base2art.Cryptography
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    
    public class Cryptor : ICryptor
    {
        private readonly ICryptorSettings settings;
        
        public Cryptor(ICryptorSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }
            
            this.settings = settings;
        }
        
        public string Decrypt<T>(string value, string salt)
            where T : SymmetricAlgorithm, new()
        {
            var dec = value.Decrypt<T>(this.settings.Secret, salt);
            return dec.HasValue ? dec.Value.AsString() : null;
        }
        
        public string Encrypt<T>(string value, string salt)
            where T : SymmetricAlgorithm, new()
        {
            var enc = value.Encrypt<T>(this.settings.Secret, salt);
            return enc.AsString();
        }
        
        public string Hash<T>(string value, string salt)
            where T : HashAlgorithm, new()
        {
            var hashedValue = string.Format(
                "{0}::__::{1}::__::{2}",
                value,
                salt,
                this.settings.Secret);
            
            return hashedValue.Hash<T>().AsString();
        }
    }
}
