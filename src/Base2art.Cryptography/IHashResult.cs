﻿namespace Base2art.Cryptography
{
    using System;
    
    public interface IHashResult
    {
        string AsString();
        
        byte[] AsByteArray();
    }
}
