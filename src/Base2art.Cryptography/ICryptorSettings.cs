﻿namespace Base2art.Cryptography
{
    public interface ICryptorSettings
    {
        string Secret { get; }
    }
}
