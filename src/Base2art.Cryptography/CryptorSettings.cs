﻿namespace Base2art.Cryptography
{
    public class CryptorSettings : ICryptorSettings
    {
        private readonly string secret;

        public CryptorSettings(string secret)
        {
            this.secret = secret;
        }

        public string Secret
        {
            get { return this.secret; }
        }
    }
}
