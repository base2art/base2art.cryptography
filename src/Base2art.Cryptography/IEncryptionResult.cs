﻿namespace Base2art.Cryptography
{
    public interface IEncryptionResult
    {
        string AsString();

        byte[] AsByteArray();
    }
}
