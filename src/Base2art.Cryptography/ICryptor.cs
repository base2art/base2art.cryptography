﻿namespace Base2art.Cryptography
{
    using System;
    using System.Security.Cryptography;

    public interface ICryptor
    {
        string Decrypt<T>(string value, string salt) where T : SymmetricAlgorithm, new();

        string Encrypt<T>(string value, string salt) where T : SymmetricAlgorithm, new();

        string Hash<T>(string value, string salt) where T : HashAlgorithm, new();
    }
}
