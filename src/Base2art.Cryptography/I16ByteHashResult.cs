﻿namespace Base2art.Cryptography
{
    using System;

    public interface I16ByteHashResult : IHashResult
    {
        Guid AsGuid();
    }
}